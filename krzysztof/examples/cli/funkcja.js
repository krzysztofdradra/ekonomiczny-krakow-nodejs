function extractArgs(restArgs) {
    const args = [];
    let pos = 0;
    restArgs.forEach((value, index) => {
        if (value.match(/^\-\-/)) {
            args[pos] = ({ key: value });
        } else {
            args[pos].value = value;
            pos++;
        }
    });
    return args;
}

exports.getArgs = extractArgs;