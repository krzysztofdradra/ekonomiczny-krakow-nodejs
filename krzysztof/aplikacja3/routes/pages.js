var { Router } = require("express");

const routes = Router()

routes.use(function (req, res, next) {
    req.user = { name: 'Guest' }
    res.locals.baseUrl = req.baseUrl
    res.locals.user = req.user
    // throw (new Error('Cant load user'))
    // next(new Error('Cant load user'))
    next()
})

routes.get("/", function (req, res) {
    res.render('pages/index', {
        // user: req.user
    })
});

routes.get("/signin", function (req, res) {
    res.render('pages/signin', { email: '', message: '' })
});

routes.get("/signup", function (req, res) {
    res.render('pages/signup')
});

routes.get("/contact", function (req, res) {
    res.render('pages/contact', { email: '', message: '', errors: [] })
});

routes.post("/contact", function (req, res) {
    const { email = '', message = '' } = req.body
    let errors = []
    if (!email || !message) {
        errors.push('Invalid data')
    }

    if (errors.length) {
        res.render('pages/contact', { email, message, errors })
    } else {
        res.redirect('/pages/contact')
        // res.send(`<h1> Success </h1>`)
    }
});

// routes.post("/contact", function (req, res) {
//     const { email, message } = req.query
// })


module.exports = routes
