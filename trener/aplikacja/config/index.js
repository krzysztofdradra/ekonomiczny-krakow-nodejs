let morgan = require('morgan')
let express = require('express')
let session = require('express-session')


module.exports = function (app) {

    app.set('view engine', 'ejs');
    app.set('views', './views')
    // https://ejs.co/#docs
    app.set('view options', {});

    app.use(express.urlencoded({ extended: true }))
    app.use(express.json({}))
    app.use(express.static('./public', {}))
    app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

    app.set("trust proxy", 1); // trust first proxy
    app.use(
        session({
            secret: process.env.SESSION_SECRET,
            resave: false,
            // https://www.npmjs.com/package/connect-mongo
            // clientPromise,
            // dbName: 'test-app'
            // store: MongoStore.create(options)
            saveUninitialized: true,
            cookie: { path: '/', httpOnly: false, secure: false, maxAge: null }
        })
    );

    app.use(function (req, res, next) {
        req.user = { name: 'Guest' }
        res.locals.baseUrl = req.baseUrl
        res.locals.user = req.user
        // throw (new Error('Cant load user'))
        // next(new Error('Cant load user'))
        next()
    })


}
