const { Router } = require("express");

const fs = require('fs')
const path = require('path')


const routes = Router()

routes.get('/list/:dir?', (req, res) => {

    fs.readdir(path.join('./public', req.params.dir || ''), {
        // withFileTypes: true
    }, (err, result) => {
        if (err) return res.status(500).send(err)
        res.json(result)
    })

    /* 
    fetch('http://localhost:3000/files/',{})
    .then(res => res.json())
    .then(console.log)
*/
})

routes.delete('/:fileName', (req, res) => {

    fs.unlink(path.resolve('./public', req.params.fileName), (err) => {
        if (err) return res.status(500).send(err)
        res.json({ message: 'ok' })
    })
    /* 
        fetch('http://localhost:3000/files/text copy.txt',{
            method:'DELETE'
        })
        .then(res => res.json())
        .then(console.log)
    */
})


routes.get('/:fileName', (req, res) => {
    // TODO: send file
    // res.set('Content-Disposition', 'attachment; filename="filename.txt"')
    // const content = fs.readFileSync('./public/text.txt')
    const fileName = path.resolve(process.cwd(), 'public', req.params.fileName)

    // res.send(content)
    // res.attachment()
    res.download(fileName, fileName, {})
    // res.sendFile()
})


module.exports = routes