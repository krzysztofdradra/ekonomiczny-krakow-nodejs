const os = require('os');
const { program } = require('commander');

console.log("Witaj w moim CLI:");

program.command('params')
    .description('show params')
    .option('-p, --pepper <value>', 'add pepper',)
    .requiredOption('-s, --salt <value', 'add salt')
    .action(function (str) {
        console.log('pepper', this.opts().pepper);
        console.log('salt', this.opts().salt);
    })

program.command('paths')
    .description('show paths')
    .action(() => {
        const [nodePath, scriptPath, ...restArgs] = process.argv;
        console.log(`Script Path: 
        ${scriptPath}`)
        console.log(`Current working dir: 
        ${process.cwd()} `);
        console.log(`Dirname: 
        ${__dirname} `);
        console.log(`Filename: 
        ${__filename} `);
        // Template strings " ` " - backtick
        console.log(`Node path: 
            "${process.execPath}" `);
    })


program.command('info')
    .description('show info')
    .action(() => {
        console.log("Processor: " + process.arch);

        console.log(`CPU: 
            ${process.cpuUsage().user / 1024} KB`);
        console.log(`Mem: 
            ${process.memoryUsage().heapUsed / 1024} KB`);
    });



program.parse();


module.exports = {}